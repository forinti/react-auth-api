<?php

namespace App\Http\Controllers;

use App\Token;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;



class ApiController extends Controller
{

    public $headers = [
        'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
        'Access-Control-Allow-Headers' => 'Content-Type, X-Auth-Token, Origin, Authorization',
        'Access-Control-Allow-Origin' => '*',
    ];


    public function __construct()
    {
        $this->middleware('cors');
    }

    public function register(Request $request)
    {
        $request->validate([
            'login' => 'required|max:170|unique:users',
            'password' => 'required|min:6|max:170',
            'confirmPassword' => 'required|min:6|max:160|same:password',
        ]);

        $user = new User();
        $user->login = $request->login;
        $user->password = bcrypt($request->password);
        $user->save();
        $token = new Token();
        $token->user_id = $user->id;
        $token->generateToken();
        $token->save();
        $response = response()->json(['user' => $user, 'token' => $token->token]);

        $keys = array_keys($this->headers);
        foreach ($keys as $key) {
            $response->header($key, $this->headers[$key]);
        }
        return $response;

    }

    public function auth(Request $request)
    {

        $rules = [
            'login' => 'required|max:170|exists:users',
            'password' => 'required|min:6',
        ];

        $validator = Validator::make($request->all(), [
            'login' => 'required|max:170|exists:users',
            'password' => 'required|min:6',
        ]);

        $keys = array_keys($this->headers);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422)->header($keys[0], $this->headers[$keys[0]])
                ->header($keys[1], $this->headers[$keys[1]])
                ->header($keys[2], $this->headers[$keys[2]]);
        }

        $user = User::where('login', $request->login)->first();
        $isMatching = Hash::check($request->password, $user->password);
        if (!$isMatching) {
            $validator->getMessageBag()->add('password', 'The password you entered is incorrect.');
            return response()->json(['errors' => $validator->errors()], 422)->header($keys[0], $this->headers[$keys[0]])
                ->header($keys[1], $this->headers[$keys[1]])
                ->header($keys[2], $this->headers[$keys[2]]);

        }
        $token = $user->token;
        if ($token->isExpired())
            $token->generateToken();


        $keys = array_keys($this->headers);
        $userWithoutToken = User::whereHas('token', function ($q) use ($token) {
            $q->where('token', $token->token);
        })->first();
        $response = response()->json(['user' => $userWithoutToken, 'token' => $token->token]);
        foreach($keys as $key) {
            $response->header($key, $this->headers[$key]);
        }
        return $response;
    }

    public function getUserByToken(Request $request) {
        $request->validate([
            'token'=>'required|max:255|exists:tokens,token'
        ]);
        $token = Token::where('token',$request->token)->first();
        $user = $token->user;
        $response = response()->json(['user'=>$user]);
        $keys = array_keys($this->headers);
        foreach($keys as $key) {
            $response->header($key, $this->headers[$key]);
        }
        return $response;
    }

}

?>

