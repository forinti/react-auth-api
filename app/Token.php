<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Token extends Model
{
    public function isExpired()
    {
        return Carbon::now() > new Carbon($this->expires_at);
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function generateToken()
    {
        $this->expires_at = Carbon::now()->addDays(7);
        $this->token = md5(time().rand());
        $this->save();
    }
}
